package model.logic;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.LinkedQueue;
import model.data_structures.LinkedStack;
import model.logic.TripsUber;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	private IArregloDinamico datos;

	private LinkedStack<TripsUber> pilaViajes;
	private LinkedQueue<TripsUber> colaViajes;

	private static final String SAMPLE_CSV_FILE_PATH = "./data/info.csv";





	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos = new ArregloDinamico(7);

	}

	public void cargar()
	{
		Reader reader;
		try {
			reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
			CSVReader csvReader = new CSVReader(reader);

			String[] nextRecord;
			csvReader.readNext();
			while ((nextRecord = csvReader.readNext()) != null) {
				double sourceid = Double.parseDouble(nextRecord[0]);
				double dstid = Double.parseDouble(nextRecord[1]);
				double hod = Double.parseDouble(nextRecord[2]);
				double mean_travel_time = Double.parseDouble(nextRecord[3]);
				double standard_deviation_travel_time = Double.parseDouble(nextRecord[4]);
				double geometric_mean_travel_time = Double.parseDouble(nextRecord[5]);
				double geometric_standard_deviation_travel_time = Double.parseDouble(nextRecord[6]);




				//demas atributos

				TripsUber t = new TripsUber( sourceid,   dstid,  hod,  mean_travel_time,  standard_deviation_travel_time,  geometric_mean_travel_time,  geometric_standard_deviation_travel_time);

				pilaViajes.push(t);
				colaViajes.enqueue(t);
			}



		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Constructor del modelo del mundo con capacidad dada
	 * @param tamano
	 */
	public MVCModelo(int capacidad)
	{
		datos = new ArregloDinamico(capacidad);
	}

	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return datos.darTamano();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(String dato)
	{	
		datos.agregar(dato);
	}

	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public String buscar(String dato)
	{
		return datos.buscar(dato);
	}

	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public String eliminar(String dato)
	{
		return datos.eliminar(dato);
	}

	public void metodo1(){

	}


	public static void main(String[] args) {

		MVCModelo m = new MVCModelo();
	}
	


}
