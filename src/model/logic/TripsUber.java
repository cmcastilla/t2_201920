package model.logic;


public class TripsUber {
	private double sourceid;
	private double dstid;
	private double hod;
	private double mean_travel_time;
	private double  standard_deviation_travel_time;
	private double geometric_mean_travel_time;
	private double geometric_standard_deviation_travel_time;
	

    public TripsUber() {

    }

    public TripsUber(double sourceid,  double dstid, double hod, double mean_travel_time, double standard_deviation_travel_time, double geometric_mean_travel_time, double geometric_standard_deviation_travel_time  ) {
        this.sourceid = sourceid;
        this.dstid = dstid;
        this.hod = hod;
        this.mean_travel_time = mean_travel_time;
        this.standard_deviation_travel_time = standard_deviation_travel_time;
        this.geometric_mean_travel_time = geometric_mean_travel_time;
        this.geometric_mean_travel_time =geometric_mean_travel_time;
    }

    public double getName() {
        return sourceid;
    }
    public double getDstid() {
        return dstid;
    }public double getHod() {
        return hod;
    }public double getMean_travel_time() {
        return mean_travel_time;
    }public double getStandard_deviation_travel_time() {
        return standard_deviation_travel_time;
    }public double getGeometric_mean_travel_time() {
        return geometric_mean_travel_time;
    }public double getGeometric_standard_deviation_travel_time() {
        return geometric_standard_deviation_travel_time;
    }
}
